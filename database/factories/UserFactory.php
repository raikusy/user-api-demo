<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'mobile' => '01'. $faker->randomElement([3, 4, 7, 8, 9]). rand(10000000, 99999999),
        'gender' => $faker->randomElement(['male', 'female', 'others', 'don\'t know']),
        'age' => rand(20, 70),
        'email_verified_at' => now(),
        'password' => bcrypt('secret'), // secret
        'active' => (bool) (rand(1, 1000) % 3), // one third active user
        'remember_token' => Str::random(10),
    ];
});
