<?php

namespace App\Filters;

use App\User;

class UserFilter {
    /**
     * Main Eloquent Query Object
     *
     * @var \Illuminate\Database\Eloquent\Builder
     */
    protected $query;

    public static function by(array $filters)
    {
        $instance = new static;
        return $instance->filter($filters);
    }

    public function filter(array $filters)
    {
        $this->query = new User;
        foreach ($filters as $filter => $value) {
            if(method_exists($this, $filter)){
                $this->query = $this->{$filter}($value);
            }
        }
        return $this->query;
    }

    protected function age($age)
    {
        if(strpos($age, ':') !== false){
            return $this->query->whereBetween('age', explode(':', $age));
        }
        return $this->query->where('age', $age);
    }

    protected function gender($gender)
    {
        return $this->query->where('gender', $gender);
    }

    protected function email($email)
    {
        return $this->query->where('email', $email);
    }

    protected function active(bool $active)
    {
        return $this->query->where('active', $active);
    }

    protected function verifier(bool $verified)
    {
        if($verified){
            return $this->query->whereNotNull('email_verified_at');
        }
        return $this->query->whereNull('email_verified_at');
    }
}