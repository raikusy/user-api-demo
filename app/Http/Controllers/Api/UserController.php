<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Filters\UserFilter;
use App\User;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = UserFilter::by($request->all())->get();
        return $users;
    }

    public function show(User $user)
    {
        return $user;
    }
}
